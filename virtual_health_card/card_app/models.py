from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.

class Patient(models.Model):
    ssn = models.CharField(max_length=20)
    pin = models.CharField(max_length=4)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    date_of_birth = models.DateField()
    citizenship = models.CharField(max_length=3)
    phone_nr = models.CharField(max_length=15)
    email = models.EmailField(max_length=50)
    insurance = models.ForeignKey(
        'Insurance',
        on_delete=models.DO_NOTHING,
    )
    general_practitioner = models.ForeignKey(
        'Doctor',
        on_delete=models.DO_NOTHING,
    )

    def __str__(self):
        return self.first_name + " " + self.last_name


class Doctor(models.Model):
    authorisation = models.CharField(max_length=20)
    pin = models.CharField(max_length=4)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    type = models.CharField(max_length=50)
    speciality = models.ForeignKey(
        'Speciality',
        on_delete=models.DO_NOTHING,
    )

    def __str__(self):
        return self.first_name + " " + self.last_name + " (" + self.speciality.name + ")"


class Pharmacist(models.Model):
    authorisation = models.CharField(max_length=20)
    pin = models.CharField(max_length=4)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    pharmacy = models.ForeignKey(
        'Pharmacy',
        on_delete=models.DO_NOTHING,
    )


    def __str__(self):
        return self.first_name + " " + self.last_name + " (" + self.pharmacy.name + ")"


class Consultation(models.Model):
    ssn = models.CharField(max_length=20)
    authorisation = models.CharField(max_length=20)
    date = models.DateField()
    clinic_name = models.CharField(max_length=50)
    clinic_type = models.CharField(max_length=50)
    consultation_report = models.CharField(max_length=1000)
    doctor = models.ForeignKey(
        'Doctor',
        on_delete=models.DO_NOTHING,
    )

    patient = models.ForeignKey(
        'Patient',
        on_delete=models.DO_NOTHING,
    )

    def __str__(self):
        return str(self.date) + " " + self.clinic_name + " (" + self.doctor.speciality.name + ")"


class Prescription(models.Model):
    date = models.DateField()
    pick_up_date = models.DateField()
    series = models.CharField(max_length=20)
    number = models.CharField(max_length=20)
    consultation = models.ForeignKey(
        'Consultation',
        on_delete=models.DO_NOTHING, )
    # insurance_type = models.CharField(max_length=50)
    diagnostic_code = models.CharField(max_length=10)
    diagnostic_type = models.CharField(max_length=50)
    active_substance = models.CharField(max_length=50)
    pharmaceutical_form = models.CharField(max_length=30)
    concentration = models.CharField(max_length=30)
    doctor = models.ForeignKey(
        'Doctor',
        on_delete=models.DO_NOTHING,
    )
    patient = models.ForeignKey(
        'Patient',
        on_delete=models.DO_NOTHING,
    )
    pharmacist = models.ForeignKey(
        'Pharmacist',
        on_delete=models.DO_NOTHING,
    )

    def __str__(self):
        return str(
            self.date) + " " + self.patient.first_name + " " + self.patient.first_name + " (" + self.doctor.speciality.name + ")"


class Insurance(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Pharmacy(models.Model):
    name = models.CharField(max_length=30)
    def __str__(self):
        return self.name


class Role(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


# class User(AbstractUser):
#     pin = models.CharField(max_length=4)
#     role = models.ForeignKey(
#         'Role',
#         on_delete=models.DO_NOTHING,
#     )
#
#     first_name = models.CharField(max_length=50)
#     last_name = models.CharField(max_length=50)
#
#     def __str__(self):
#         return self.first_name + " " + self.last_name + "(" + self.role.name + ")"


class Speciality(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name
