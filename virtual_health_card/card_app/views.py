from django.shortcuts import render
from card_app import models


# Create your views here.


def home_page_view(request):
    context = {'card_app': models.Card_app.objects.all()}
    return render(request, 'card_app_list.html', context)


def product_details_view(request):
    # context = {'all_products': models.Product.objects.all()}
    return render(request, 'card_app_details.html')


def product_add_view(request):
    return render(request, 'card_app_add.html')
