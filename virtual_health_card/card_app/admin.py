from django.contrib import admin

# Register your models here.
from card_app import models
from card_app.models import *

admin.site.register(Patient)
admin.site.register(Insurance)
admin.site.register(Doctor)
admin.site.register(Pharmacy)
admin.site.register(Pharmacist)
admin.site.register(Consultation)
admin.site.register(Prescription)
admin.site.register(Role)
admin.site.register(Speciality)


